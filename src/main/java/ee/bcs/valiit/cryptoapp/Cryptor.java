package ee.bcs.valiit.cryptoapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public abstract class Cryptor {

    protected Map<String, String> dictionary = null;

    public Cryptor(String filePath) throws IOException {
        List<String> fileLines = readAlphabet(filePath);
        this.dictionary = generateDictionary(fileLines);
    }

    protected abstract Map<String, String> generateDictionary(List<String> fileLines);

    protected List<String> readAlphabet(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        // List<String> rows = Files.readAllLines(path);
        return Files.readAllLines(path);
    }

    public String translate(String text) {
        if (text != null) {
            StringBuilder result = new StringBuilder();
            char[] textChars = text.toUpperCase().toCharArray();
            for (char c : textChars) {
                String letter = String.valueOf(c);
                String sym = dictionary.get(letter);
                result.append(sym != null ? sym : letter);
            }
            return result.toString();
        }
        return null;
    }

}
