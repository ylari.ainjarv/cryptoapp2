package ee.bcs.valiit.cryptoapp;

import java.io.IOException;

public class CryptoApp {

    static String envelope = "";

    public static void main(String[] args) {
        try {

            // Kati saadab sõnumi
            Encryptor encryptor = new Encryptor(args[0]);
            String text = "Võti on mati all";
            String result = encryptor.translate(text);
            System.out.println("Enc: " + result);
            envelope = result;

            // Mati saab sõnumi ja teeb selle lahti
            Decryptor decryptor = new Decryptor(args[0]);
            result = decryptor.translate(envelope);
            System.out.println("Dec: " + result);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
