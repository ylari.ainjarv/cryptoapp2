package ee.bcs.valiit.cryptoapp;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Encryptor extends Cryptor {

    public Encryptor(String filePath) throws IOException {
        super(filePath);
    }

    @Override
    protected Map<String, String> generateDictionary(List<String> fileLines) {
        Map<String, String> dictionary = new HashMap<>();
        for (String fileLine : fileLines) {
            String[] lineParts = fileLine.split(", ");
            dictionary.put(lineParts[0], lineParts[1]);
        }
        return dictionary;
    }

}
