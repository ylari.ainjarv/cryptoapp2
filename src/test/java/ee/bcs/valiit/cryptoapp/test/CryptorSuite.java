package ee.bcs.valiit.cryptoapp.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({EncryptorTest.class, DecryptorTest.class})
public class CryptorSuite {
}
