package ee.bcs.valiit.cryptoapp.test;

import ee.bcs.valiit.cryptoapp.Encryptor;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class EncryptorTest {

    @Before
    public void start() {
        System.out.println("Testimine alaku ...");
    }

    @Test
    public void testTranslate() throws IOException {
        Encryptor encryptor = new Encryptor("C:\\Users\\opilane\\IdeaProjects\\cryptoapp\\src\\main\\resources\\alfabeet.txt");
        String result = encryptor.translate("VÕTI ON MATI ALL");
        System.out.println("Test käib ...");
        Assert.assertEquals("FDHŽ.PQ.RÜHŽ.ÜSS", result);
    }

    @After
    public void stop() {
        System.out.println("Testimine sai läbi");
    }

}
