package ee.bcs.valiit.cryptoapp.test;

import ee.bcs.valiit.cryptoapp.Decryptor;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class DecryptorTest {

    @Test
    public void testTranslate() throws IOException {
        Decryptor decryptor = new Decryptor("C:\\Users\\opilane\\IdeaProjects\\cryptoapp\\src\\main\\resources\\alfabeet.txt");
        String result = decryptor.translate("FDHŽ.PQ.RÜHŽ.ÜSS");
        Assert.assertEquals("VÕTI ON MATI ALL", result);
    }

}
